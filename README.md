# schat-frontend

## 介绍

仿 telegram IM即时通讯系统。使用技术栈Vue3.0+Vant+socket.io+Vue-i18n+虚拟滚动+音视频播放等。后端使用nestjs+mongodb+redis+socket.io等。

server在这里 <a href="https://gitee.com/suwenhao/schat-nestjs-server">https://gitee.com/suwenhao/schat-nestjs-server</a>

## 软件说明

1. 支持PC和H5端自适应功能，支持主题颜色更改，支持多语言设置。
2. 支持私聊群聊功能，支持文字消息发送，图片消息发送，视频消息发送，语音音频消息发送，文件发送。实时消息socket推送接收。消息通过redis推送到队列后存储到mongodb数据库中。
3. 支持搜索添加用户为好友，支持新建群聊。
4. 完成功能
  - 登录注册
  - 主页
    - 主题切换，多语言切换，添加联系人，通讯录，添加群聊
  - 个人资料更改
    - 头像，对话设置，通知设置，个人资料，邮箱验证，google二次验证，隐私设置-上线时间显示设置，查看头像显示设置，通过账号昵称添加我设置，添加好友验证设置，添加我为好友验证设置，添加为群主设置等等。
  - 私聊，群聊
    - 聊天置顶，聊天免消息打扰，好友设置黑名单，语音发送，文字图片视频音频文件消息发送，视频播放，音频播放，文件下载，表情发送，消息换行，消息引用，消息转发，消息删除，消息置顶，群聊禁言，添加群成员，消息虚拟列表滚动显示，群成员列表，群权限管理，添加管理员，群资料设置，群头像设置，群成员昵称设置，群私密公开设置，进群审核设置，群管理处申请，群静态资源列表，群公告，退出群聊，清空群消息，@群成员，等等。
5. 待完善功能
  - 多消息转发，多消息引用，消息类型搜索，删除好友，搜索公开群聊添加，邮箱验证完善，修改密码处理。
  - 待增加功能-> 消息收藏夹，消息公告显示，管理员功能，好友朋友圈，朋友圈权限，订阅号功能，订阅号后台功能管理等。

## 页面预览

<img src="./docs/images/1.png" width="880">
<img src="./docs/images/2.png" width="880">
<img src="./docs/images/3.png" width="880">
<img src="./docs/images/4.png" width="880">
<img src="./docs/images/5.png" width="880">
<img src="./docs/images/6.png" width="880">
<img src="./docs/images/7.png" width="880">
<img src="./docs/images/8.png" width="880">
<img src="./docs/images/9.png" width="880">
<img src="./docs/images/10.png" width="880">
<img src="./docs/images/11.png" width="880">
<img src="./docs/images/12.png" width="880">
<img src="./docs/images/13.png" width="880">
<img src="./docs/images/14.png" width="880">
<img src="./docs/images/15.png" width="880">
<img src="./docs/images/16.png" width="880">
<img src="./docs/images/17.png" width="880">
<img src="./docs/images/18.png" width="880">
<img src="./docs/images/19.png" width="880">
<img src="./docs/images/20.png" width="880">
<img src="./docs/images/21.png" width="880">
<img src="./docs/images/22.png" width="880">
<img src="./docs/images/23.png" width="880">
<img src="./docs/images/24.png" width="880">
<img src="./docs/images/25.png" width="880">
<img src="./docs/images/26.png" width="880">
<img src="./docs/images/27.png" width="880">
<img src="./docs/images/28.png" width="880">
<img src="./docs/images/29.png" width="880">
<img src="./docs/images/30.png" width="880">
<img src="./docs/images/31.png" width="880">
<img src="./docs/images/32.png" width="880">
<img src="./docs/images/33.png" width="880">
<img src="./docs/images/34.png" width="880">
<img src="./docs/images/35.png" width="880">
<img src="./docs/images/36.png" width="880">
<img src="./docs/images/37.png" width="880">
<img src="./docs/images/38.png" width="880">
<img src="./docs/images/39.png" width="880">
<img src="./docs/images/40.png" width="880">
<img src="./docs/images/41.png" width="880">
<img src="./docs/images/42.png" width="880">
<img src="./docs/images/43.png" width="880">
<img src="./docs/images/44.png" width="880">
<img src="./docs/images/45.png" width="880">
<img src="./docs/images/46.png" width="880">
<img src="./docs/images/47.png" width="880">
<img src="./docs/images/48.png" width="880">
<img src="./docs/images/49.png" width="880">
<img src="./docs/images/50.png" width="880">
<img src="./docs/images/51.png" width="880">
<img src="./docs/images/52.png" width="880">
<img src="./docs/images/53.png" width="880">

## 后端
  - 使用nestjs开发
  - 给出打包后的源码
  - 开发环境和运行环境配置在编写处理中
```
│  app.module.ts
│  config.schema.ts
│  main.ts
├─api
│  └─index
│      │  index.controller.ts
│      │  index.module.ts
│      │  index.service.ts
│      │  jwt.strategy.ts
│      ├─decorator
│      │      get-user.decorator.ts
│      ├─dto
│      │      auth-credentials.dto.ts
│      │      authenticator.dto.ts
│      │      credentials.dto.ts
│      │      file-upload.dto.ts
│      │      verify-google.dto.ts
│      └─interface
│              jwt-payload.interface.ts
├─common
│  ├─database
│  │      mongoose.import.module.ts
│  ├─email
│  │      email.import.module.ts
│  ├─ftp
│  │      ftp.import.module.ts
│  ├─log4js
│  │      log4js.import.module.ts
│  ├─redis
│  │      redis.import.module.ts
│  ├─session
│  │      session.import.module.ts
│  └─upload
│          upload.module.ts
├─enums
│      global.enum.ts
├─interceptor
│      response.interceptor.t
├─locales
│      en-US.ts
│      ja-JP.ts
│      zh-CN.ts
│      zh-TW.ts
├─middlewares
│      logger.middleware.ts
├─modules
│  │  entity.repository.ts
│  │
│  ├─address
│  │  │  address.module.ts
│  │  │  address.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          address.schema.ts
│  ├─apply
│  │  │  apply.interface.ts
│  │  │  apply.module.ts
│  │  │  apply.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          apply.schema.ts
│  ├─chat
│  │  │  chat.interface.ts
│  │  │  chat.module.ts
│  │  │  chat.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          chat.schema.ts
│  ├─chathistory
│  │  │  chathistory.interface.ts
│  │  │  chathistory.module.ts
│  │  │  chathistory.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          chathistory.schema.ts
│  ├─city
│  │  │  city.module.ts
│  │  │  city.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          city.schema.ts
│  ├─friend
│  │  │  friend.interface.ts
│  │  │  friend.module.ts
│  │  │  friend.repository.ts
│  │  │  index.ts
│  │  │
│  │  └─schemas
│  │          friend.schema.ts
│  ├─group
│  │  │  group.interface.ts
│  │  │  group.module.ts
│  │  │  group.repository.ts
│  │  │  index.ts
│  │  └─schemas
│  │          group.schema.ts
│  ├─groupmanage
│  │  │  groupmanage.interface.ts
│  │  │  groupmanage.module.ts
│  │  │  groupmanage.repository.ts
│  │  │  index.ts
│  │  └─schemas
│  │          groupmanage.schema.ts
│  ├─groupmember
│  │  │  groupmember.interface.ts
│  │  │  groupmember.module.ts
│  │  │  groupmember.repository.ts
│  │  │  index.ts
│  │  └─schemas
│  │          groupmember.schema.ts
│  ├─loginlog
│  │  │  index.ts
│  │  │  loginlog.module.ts
│  │  │  loginlog.repository.ts
│  │  └─schemas
│  │          loginlog.schema.ts
│  ├─notice
│  │  │  index.ts
│  │  │  notice.module.ts
│  │  │  notice.repository.ts
│  │  └─schemas
│  │          notice.schema.ts
│  ├─top
│  │  │  index.ts
│  │  │  top.module.ts
│  │  │  top.repository.ts
│  │  └─schemas
│  │          top.schema.ts
│  └─user
│      │  index.ts
│      │  user.interface.ts
│      │  user.module.ts
│      │  user.repository.ts
│      └─schemas
│              user.schema.ts
├─socket
│  │  socket.format.ts
│  │  socket.gateway.ts
│  │  socket.module.ts
│  ├─interface
│  │      services.interface.ts
│  │      socket.interface.ts
│  └─services
│          apply.service.ts
│          chat.service.ts
│          chathistory.service.ts
│          common.service.ts
│          friend.service.ts
│          group.service.ts
│          groupmanage.service.ts
│          groupmember.service.ts
│          online.service.ts
│          top.service.ts
│          user.service.ts
└─utils
    │  index.ts
    ├─email
    │      email.constants.ts
    │      email.interface.ts
    │      email.module.ts
    │      email.service.ts
    │      index.ts
    └─ftp
            ftp.constants.ts
            ftp.interface.ts
            ftp.module.ts
            ftp.service.ts
            index.ts
```