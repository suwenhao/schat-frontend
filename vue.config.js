const { defineConfig } = require("@vue/cli-service");
const Timestamp = new Date().getTime();
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");

module.exports = defineConfig({
  productionSourceMap: false,
  transpileDependencies: true,
  lintOnSave: false,
  css: {
    extract: {
        filename: `css/[name]_[hash:8].${Timestamp}.css`,
        chunkFilename: `css/[name]_[chunkhash:8].${Timestamp}.css`,
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set("vue-i18n", "vue-i18n/dist/vue-i18n.cjs.js");
    config.output.filename(`js/[name]_[hash:8].${Timestamp}.js`).end()
    config.output.chunkFilename(`js/[name]_[chunkhash:8].${Timestamp}.js`).end()
    
    if (process.env.NODE_ENV == "production") {
      config.plugin("compressionPlugin").use(
        new CompressionPlugin({
          filename: "[path].gz",
          algorithm: "gzip",
          test: /\.(js|css|png|jpeg|jpg|woff|eot|woff2|ttf|json|html)$/i,
          deleteOriginalAssets: true,
          threshold: 1024,
        })
      );
    }
  },
  configureWebpack: (config) => {
    if (process.env.NODE_ENV == "production") {
      config.optimization.minimizer = [
        new UglifyJsPlugin({
          uglifyOptions: {
            output: {
              comments: false,
            },
            compress: {
              drop_console: true,
              drop_debugger: false,
              pure_funcs: ["console.log"],
            },
            warnings: false,
          },
        }),
      ];
    }
  },
  devServer: {
    // 设置代理
    proxy: {
      "/": {
        target: process.env.VUE_APP_BASEURL,
        ws: false, // 是否启用websockets
        changOrigin: true,
        pathRewrite: {
          "^/": "",
        },
      },
    },
  },
});
