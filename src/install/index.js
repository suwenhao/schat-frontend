import mitt from "mitt";

// component
import {
  Form,
  Cell,
  CellGroup,
  Field,
  Button,
  Calendar,
  Row,
  Col,
  NavBar,
  Slider,
  Progress,
  Dialog,
  Icon,
  Popup,
  Switch,
  Image as VanImage,
  Popover,
  RadioGroup,
  CheckboxGroup,
  Checkbox,
  Radio,
  Tab,
  Tabs,
  Swipe,
  SwipeItem,
  IndexBar,
  IndexAnchor,
  Loading,
  ImagePreview,
} from "vant";

import VueAvatar from "@/components/base/imgUpdate/avatar.vue";
import VueAvatarScale from "@/components/base/imgUpdate/avatarScale.vue";
import UploadProgress from "@/components/base/uploadProgress/index.vue";
import BaseAlert from "@/components/base/alert/index.vue";
import NoAata from "@/components/base/nodata/index.vue";
import AvatarBox from "@/components/base/avatar/avatar.vue";
import SetAvatar from "@/components/base/avatar/setAvatar.vue";

// global function
import * as Util from "@/utils";
import * as Constant from "@/utils/constant";
import { Storage, SStorage } from "@/utils/storage";
import { websql } from "@/utils/websql";

// componennt obj
const vantComponent = {
  Form,
  Cell,
  CellGroup,
  Field,
  Button,
  Calendar,
  Row,
  Col,
  NavBar,
  Slider,
  Progress,
  Dialog,
  Icon,
  Popup,
  Switch,
  VanImage,
  Popover,
  RadioGroup,
  CheckboxGroup,
  Checkbox,
  Radio,
  Tab,
  Tabs,
  Swipe,
  SwipeItem,
  IndexBar,
  IndexAnchor,
  Loading,
  ImagePreview,
};

//  global function obj
const globalFunction = {
  storage: Storage,
  sstorage: SStorage,
  constant: Constant,
  util: Util,
};

export default {
  install(app, store, router) {
    Object.keys(vantComponent).forEach((key) => {
      app.use(vantComponent[key]);
    });
    Object.keys(globalFunction).forEach((key) => {
      app.config.globalProperties["$" + key] = globalFunction[key];
    });

    app.component("vue-avatar", VueAvatar);
    app.component("vue-avatar-scale", VueAvatarScale);
    app.component("upload-progress", UploadProgress);
    app.component("base-alert", BaseAlert);
    app.component("no-data", NoAata);
    app.component("avatar-box", AvatarBox);
    app.component("set-avatar", SetAvatar);
    app.config.globalProperties.$bus = mitt();

    app.config.globalProperties.$logout = () => {
      Storage.clear();
      SStorage.clear();
      // 清除聊天缓存
      (async () => {
        let keys = await websql.keys();
        keys = keys.filter((item) => item.indexOf("62") != -1);
        keys.forEach((item) => {
          websql.remove(item);
        });
      })();
      const socket = app.config.globalProperties.$socket.init();
      socket.disconnect();
      location.href = "/";
    };
  },
};
