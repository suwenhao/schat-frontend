import {
  FRIEND_INFO,
  GROUP_INFO,
  SELECT_CHAT,
  CHAT_LIST,
} from "@/utils/constant";
import { Storage } from "@/utils/storage";

export default {
  namespaced: true,
  state: {
    chatMessages: [],
    // 申请加我的列表
    friendApplyList: [],
    // 我的申请列表
    myFriendApplyList: [],
    // 选中的聊天
    selectChat: Storage.get(SELECT_CHAT) || "",
    // 聊天列表
    chatList: Storage.get(CHAT_LIST) || [],
    // 好友列表
    friendList: [],
    // 好友信息
    friendInfo: Storage.get(FRIEND_INFO) || {},
    // 群列表
    groupList: [],
    // 群信息
    groupInfo: Storage.get(GROUP_INFO) || {},
    // 群申请列表
    groupApplyList: [],
    // 群成员列表
    groupMemberList: [],
    // 群管理员
    groupManageList: [],
    // 查看某个用户的信息
    lookUserInfo: {},
    // 查看的类型
    lookType: "",
    // 置顶消息列表
    topList: [],
  },
  getters: {},
  actions: {},
  mutations: {
    setChatMessages(state, newState) {
      state.chatMessages = newState;
    },
    setFriendApplyList(state, newState) {
      state.friendApplyList = newState;
    },
    setMyFriendApplyList(state, newState) {
      state.myFriendApplyList = newState;
    },
    setChatList(state, newState) {
      state.chatList = newState;
      Storage.set(CHAT_LIST, newState);
    },
    setFriendList(state, newState) {
      state.friendList = newState;
    },
    setFriendInfo(state, newState) {
      state.friendInfo = newState;
      Storage.set(FRIEND_INFO, newState);
    },
    setSelectChat(state, newState) {
      state.selectChat = newState;
      Storage.set(SELECT_CHAT, newState);
    },
    setGroupInfo(state, newState) {
      state.groupInfo = newState;
      Storage.set(GROUP_INFO, newState);
    },
    setGroupList(state, newState) {
      state.groupList = newState;
    },
    setGroupApplyList(state, newState) {
      state.groupApplyList = newState;
    },
    setGroupMemberList(state, newState) {
      state.groupMemberList = newState;
    },
    setGroupManageList(state, newState) {
      state.groupManageList = newState;
    },
    setLookUserInfo(state, newState) {
      state.lookUserInfo = newState;
    },
    setLookType(state, newState) {
      state.lookType = newState;
    },
    setTopList(state, newState) {
      state.topList = newState;
    }
  },
};
