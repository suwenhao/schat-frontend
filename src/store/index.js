import * as Vuex from "vuex";
import home from "./models/home";
import { Storage } from "@/utils/storage";
import {
  USERINFO,
  THEME,
  LANGUAGE,
  DESKTOP_NOTICE,
  PLAY_BEEP,
  PRIMARY,
} from "@/utils/constant";
import { getBrowserLang } from "@/utils";
import bus from "@/utils/bus";
import { websql } from "@/utils/websql";

console.log();

export default Vuex.createStore({
  state: {
    locale: getBrowserLang() || process.env.VUE_APP_LANGUAGE, // 语言
    theme: Storage.get(THEME) || process.env.VUE_APP_THEME, // 主题
    desknotice: process.env.VUE_APP_DESKTOP_NOTICE, // 桌面提示
    playbeep: process.env.VUE_APP_PLAY_BEEP, // 播放提示音
    primary: process.env.VUE_APP_PRIMARY, // 主色

    loginUserInfo: Storage.get(USERINFO) || null, // 登录信息
    fileUploadProgress: 0, // 上传进度
    ismobile: false, // 是否移动端

    showProgress: false, // 上传进度显示
    leftMenuShow: false, // 左侧菜单显示
    contactShow: false, // 联系人弹窗
    addContactShow: false, // 添加联系人弹窗
    friendApplyShow: false, // 好友申请列表弹窗
    settingsShow: false, // 设置弹窗
    adminShow: false, // 管理员才有的弹窗
    noticeShow: false, // 管理员才有的弹窗
    selectContactShow: false, // 群聊选择联系人弹窗
    addGroupShow: false, // 添加群聊弹窗
    userInfoShow: false, // 用户信息弹窗
    groupInfoShow: false, // 群信息弹窗
    groupApplyShow: false, // 群申请列表弹窗
    groupShow: false, // 群列表弹窗
    addGroupMemberShow: false, // 添加群成员弹窗
    editGroupInfoShow: false, // 修改群资料弹窗
    groupMemberShow: false, // 群成员列表弹窗
    forwardShow: false,
  },
  getters: {
    language(state) {
      return state.locale;
    },
    colorTheme(state) {
      return state.theme;
    },
  },
  mutations: {
    setIsMobile(state, newState) {
      state.ismobile = newState;
    },
    // 设置语言
    setLocale(state, newState) {
      state.locale = newState;
      bus.emit("changeLanguage", state.locale);
      (async () => await websql.set(LANGUAGE, newState))();
    },
    // 设置主题
    setTheme(state, newState) {
      state.theme = newState;
      Storage.set(THEME, newState);
      (async () => await websql.set(THEME, newState))();
    },
    // 设置桌面提示
    setDesktopNotice(state, newState) {
      state.desknotice = newState;
      (async () => await websql.set(DESKTOP_NOTICE, newState))();
    },
    // 设置提示音播放
    setPlayBeep(state, newState) {
      state.playbeep = newState;
      (async () => await websql.set(PLAY_BEEP, newState))();
    },
    // 设置主色
    setPrimary(state, newState) {
      state.primary = newState;
      (async () => await websql.set(PRIMARY, newState))();
    },

    // 设置登录信息
    setLoginUserInfo(state, newState) {
      state.loginUserInfo = newState;
      Storage.set(USERINFO, newState);
    },
    // 设置上传进度
    setFileUploadProgress(state, newState) {
      state.fileUploadProgress = newState;
    },
    // 设置上传进度打开关闭
    setShowProgress(state, newState) {
      state.showProgress = newState;
    },
    // 设置左侧菜单打开关闭
    setLeftMenuShow(state, newState) {
      state.leftMenuShow = newState;
    },
    // 设置联系人弹窗打开关闭
    setContactShow(state, newState) {
      state.contactShow = newState;
    },
    // 设置添加联系人弹窗打开关闭
    setAddContactShow(state, newState) {
      state.addContactShow = newState;
    },
    // 设置好友申请弹窗打开关闭
    setFriendApplyShow(state, newState) {
      state.friendApplyShow = newState;
    },
    // 设置设置弹窗打开关闭
    setSettingsShow(state, newState) {
      state.settingsShow = newState;
    },
    // 设置管理员弹窗打开关闭
    setAdminShow(state, newState) {
      state.adminShow = newState;
    },
    // 设置群里选择联系人弹窗打开关闭
    setSelectContactShow(state, newState) {
      state.selectContactShow = newState;
    },
    // 设置添加群聊弹窗打开关闭
    setAddGroupShowShow(state, newState) {
      state.addGroupShow = newState;
    },
    // 设置管理员弹窗打开关闭
    setNoticeShow(state, newState) {
      state.noticeShow = newState;
    },
    // 设置用户信息弹窗打开关闭
    setUserInfoShow(state, newState) {
      state.userInfoShow = newState;
    },
    // 设置群信息弹窗打开关闭
    setGroupInfoShow(state, newState) {
      state.groupInfoShow = newState;
    },
    // 设置群申请弹窗打开关闭
    setGroupApplyShow(state, newState) {
      state.groupApplyShow = newState;
    },
    // 设置群列表弹窗打开关闭
    setGroupShow(state, newState) {
      state.groupShow = newState;
    },
    // 设置添加群成员弹窗打开关闭
    setAddGroupMemberShow(state, newState) {
      state.addGroupMemberShow = newState;
    },
    // 设置修改群资料弹窗打开关闭
    setEditGroupInfoShow(state, newState) {
      state.editGroupInfoShow = newState;
    },
    // 设置查看群成员弹窗打开关闭
    setGroupMemberShow(state, newState) {
      state.groupMemberShow = newState;
    },
    // 设置转发弹窗打开关闭
    setForwardShow(state, newState) {
      state.forwardShow = newState;
    },
  },
  modules: {
    home,
  },
});
