import * as VueRouter from "vue-router";
// com
import { setToken } from "@/api";
import store from "@/store";
import { Storage } from "@/utils/storage";
import { TOKEN } from "@/utils/constant";

const Home = () => import("@/views/Home.vue");

const routes = [
  {
    path: "/",
    redirect: "/home"
  },
  {
    path: "/home",
    name: "home",
    component: Home,
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/Login.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("@/views/Login.vue"),
  },
];

const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  routes,
});
// 导航守卫设置请求token
router.beforeEach(async (to, from, next) => {
  const token = Storage.get(TOKEN);
  if (token) {
    const loginUserInfo = store.state.loginUserInfo;
    if (loginUserInfo) {
      setToken(token);
      if (to.name === "login") {
        next('/');
      } else {
        next();
      }
    } else {
      next({ name: "login" });
    }
  } else {
    if (to.name === "login") {
      next();
    } else {
      next({ name: "login" });
    }
  }
});

export default router;
