import { Toast } from "vant";
import axios from "axios";
import router from "@/router";
import store from "@/store";
import { TOKEN, LANGUAGE_OBJ } from "@/utils/constant";
import { Storage } from "@/utils/storage";
import { createI18n } from "vue-i18n";
import { messages } from "@/i18n";
import bus from "@/utils/bus";

Toast.allowMultiple();
let toast = null;

const ToastFn = (text) => {
  toast = Toast(text)
}
export const i18n = createI18n({
  legacy: false,
  locale: store.state.locale, // set locale
  fallbackLocale: store.state.locale,
  messages, // set locale messages
});

bus.on("changeLanguage", (locale) => {
  i18n.global.locale = locale;
});

const createRequest = () => {
  let option = {
    withCredentials: true,
    timeout: 1000000000000000,
  };
  return axios.create(option);
};
const api = createRequest();

api.interceptors.request.use(
  (config) => {
    // 这里的config包含每次请求的内容
    let token = Storage.get(TOKEN);
    i18n.global.locale = store.state.locale;
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
    } else {
      let { name, query } = router.currentRoute;
      if (name === "login") {
        router.push({ name: "login", query });
      } else if (name === "register") {
        router.push({ name: "register", query });
      }
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);
api.interceptors.response.use(
  (res) => {
    toast?.clear();
    try {
      if (res.data?.code != 1) {
        let msgObj = Storage.get(LANGUAGE_OBJ);
        ToastFn(msgObj[res.data.msg]); // 警告通知
      }
    } catch (error) {
      ToastFn(i18n.global.t("global.networkError")); // 警告通知
    }
    return res;
  },
  (err) => {
    if (err.response) {
      toast?.clear();
      switch (err.response.status) {
        case 500:
          clearInterval(window.checkTokenTimer);
          ToastFn(i18n.global.t("global.networkError")); // 警告通知
          return err;
        case 401:
          ToastFn(i18n.global.t("global.loginInvalid")); // 警告通知
          Storage.clear();
          router.push({ name: "login" });
          return err;
      }
    }
    return err;
  }
);

export default {
  get(url, params = {}, config = {}) {
    return api.get(url, { ...config, params });
  },
  post(url, params = {}, config = {}) {
    return api.post(url, params, {
      ...config,
    });
  },
  put(url, params = {}, config = {}) {
    return api.put(url, params, config);
  },
  setToken(token) {
    api.defaults.headers["Authorization"] = "Bearer " + token;
  },
};
