import http from "@/api/http";
import store from '@/store'

export const setToken = (token) => {
  http.setToken(token);
};
export const captcha = (random) => {
  return process.env.VUE_APP_BASEURL + "/api/captcha?randomStr=" + random;
};
export const checkToken = (params) => {
  return http.post("/api/t", params);
}
export const getLocales = ({ lang }) => {
  return http.get(`/api/locales/${lang}`);
}
export const signIn = (params) => {
  return http.post("/api/signin", params, {});
};
export const signUp = (params) => {
  return http.post("/api/signup", params, {});
};
export const sendEmailCode = (params) => {
  return http.post("/api/send-email-code", params);
}
export const verifyEmailCode = (params) => {
  return http.post("/api/verify-email-code", params);
}
export const authenticator = (params) => {
  return http.get("/api/authenticator", params);
}
export const getGoogleTotpUri = (params) => {
  return http.get("/api/google-auth", params);
}
export const verifyGoogleCode = (params) => {
  return http.post("/api/verify-google", params);
}
export const uploadSingleFile = (params) => {
  return http.post("/api/upload-single-file", params, {
    onUploadProgress: e => {
      const {loaded, total} = e;
      // 使用本地 progress 事件
      if (e.lengthComputable) {
        let progress = (loaded / total) * 100;
        // console.log(loaded, total, `${progress}%`);
        store.commit('setFileUploadProgress', progress)
      }
    }
  });
};
export const uploadSingleBase64 = (params) => {
  return http.post("/api/upload-single-base64", params, {
    onUploadProgress: e => {
      const {loaded, total} = e;
      // 使用本地 progress 事件
      if (e.lengthComputable) {
        let progress = (loaded / total) * 100;
        // console.log(loaded, total, `${progress}%`);
        store.commit('setFileUploadProgress', progress)
      }
    }
  });
};
