import { getCurrentInstance, computed } from "vue";
import { useStore } from "vuex";
import { useRoute, useRouter } from "vue-router";
import { useI18n } from "vue-i18n";
import { websql } from "@/utils/websql";
import { Toast, Dialog } from "vant";
import lodash from "lodash";
import {
  IS_ADD_MEMBER,
  IS_ADMIN,
  IS_AUDIT,
  IS_BLACK,
  IS_FORBIDDEN,
  IS_FORWARD,
  IS_MANAGE,
  IS_SET_APPLY,
  IS_SET_BLACK,
  IS_SET_DELETE,
  IS_SET_MANAGE,
  IS_SET_NOTICE,
  IS_SET_SETTING,
  IS_SET_TOP,
  IS_SHOWNICK,

  LANGUAGE_OBJ
} from "@/utils/constant";

import FileSaver from "file-saver";
import imageAspectRatio from "image-aspect-ratio";
import mime from "mime";
import path from "path-browserify";
import {
  getIconForFile,
  getIconForFolder,
} from "vscode-icons-js";

Toast.allowMultiple();

// 字符串替换
const $formatLocale = (str, key, val) => {
  let reg = new RegExp(key, "i");
  return str.replace(reg, val);
};
export const useGlobal = () => {
  const store = useStore();
  const router = useRouter();
  const route = useRoute();
  const { proxy } = getCurrentInstance();
  const { $bus, $util, $storage, $sstorage, $constant } = proxy;
  const { locale, t: $t } = useI18n();
  const ismobile = computed(() => store.state.ismobile);
  // 主题语言等
  const theme = computed(() => store.state.theme);
  const primary = computed(() => store.state.primary);
  const desknotice = computed(() => store.state.desknotice);
  const playbeep = computed(() => store.state.playbeep);
  const localed = computed(() => store.state.locale);
  // 弹窗显示隐藏
  const addContactShow = computed(() => store.state.addContactShow);
  const adminShow = computed(() => store.state.adminShow);
  const contactShow = computed(() => store.state.contactShow);
  const friendApplyShow = computed(() => store.state.friendApplyShow);
  const leftMenuShow = computed(() => store.state.leftMenuShow);
  const noticeShow = computed(() => store.state.noticeShow);
  const selectContactShow = computed(() => store.state.selectContactShow);
  const settingsShow = computed(() => store.state.settingsShow);
  const userInfoShow = computed(() => store.state.userInfoShow);
  const groupInfoShow = computed(() => store.state.groupInfoShow);
  const groupApplyShow = computed(() => store.state.groupApplyShow);
  const groupShow = computed(() => store.state.groupShow);
  const addGroupMemberShow = computed(() => store.state.addGroupMemberShow);
  const editGroupInfoShow = computed(() => store.state.editGroupInfoShow);
  const groupMemberShow = computed(() => store.state.groupMemberShow);
  const forwardShow = computed(() => store.state.forwardShow);

  // 关闭所有窗口
  const $closeAllAlert = () => {
    store.commit("setLeftMenuShow", false);
    store.commit("setContactShow", false);
    store.commit("setAddContactShow", false);
    store.commit("setFriendApplyShow", false);
    store.commit("setAdminShow", false);
    store.commit("setSelectContactShow", false);
    store.commit("setAddGroupShowShow", false);
    store.commit("setNoticeShow", false);
    store.commit("setUserInfoShow", false);
    store.commit("setGroupInfoShow", false);
    store.commit("setGroupApplyShow", false);
    store.commit("setGroupShow", false);
    store.commit("setAddGroupMemberShow", false);
    store.commit("setEditGroupInfoShow", false);
    store.commit("setGroupMemberShow", false);
  };
  // 时间翻译
  const $formatTime = () => {
    return {
      justRecently: $t("global.justRecently"),
      yesterday: $t("global.yesterday"),
      beforeday: $t("global.beforeday"),
      week1: $t("global.week1"),
      week2: $t("global.week2"),
      week3: $t("global.week3"),
      week4: $t("global.week4"),
      week5: $t("global.week5"),
      week6: $t("global.week6"),
      week7: $t("global.week7"),
    };
  };
  // 拼接图片视频等静态资源地址
  const $formatStatic = (src) => {
    if (!src) return "";
    if (src.indexOf("base64") != -1 || src.indexOf("http") != -1) {
      return src;
    }
    if (process.env.VUE_APP_ENV != "dev") {
      return `${location.protocol}//${location.hostname}${process.env.VUE_APP_STATICURL}${src}`;
    } else {
      return process.env.VUE_APP_STATICURL + src;
    }
  };
  // 深度copy
  const $deepclone = (obj) => {
    return JSON.parse(JSON.stringify(obj));
  };
  // 文件下载
  const $downloadFile = (src, filename) => {
    FileSaver.saveAs($formatStatic(src), filename);
  };
  // 计算等比宽高
  const $formatRatio = (width, height, ratio) => {
    return imageAspectRatio.calculate(width, height, ratio, ratio);
  };
  // 文件 mime类型
  const $formatMime = (name) => {
    if (!name) return "";
    return mime.getType(name);
  };
  // 文件图标显示
  const $formatFileIcon = (filename, type = "file") => {
    if (type == "file") {
      return require(`@/assets/icons/${getIconForFile(filename)}`);
    } else if (type == "folder") {
      return require(`@/assets/icons/${getIconForFolder(filename)}`);
    }
  };
  // 缩略图
  const $formatThumbnail = (src) => {
    if (!src) return;
    if (src.indexOf("blob") != -1) {
      return src;
    }
    let { name, ext, dir } = path.parse(src);
    return `${dir}/${name}__thumbnail${ext}`;
  };
  return {
    store,
    router,
    route,
    locale,
    websql,
    proxy,

    $bus,
    $util,
    $storage,
    $sstorage,
    $constant,
    // 主题通知开启等

    theme,
    primary,
    localed,
    playbeep,
    ismobile,
    desknotice,

    addContactShow,
    adminShow,
    contactShow,
    friendApplyShow,
    leftMenuShow,
    noticeShow,
    selectContactShow,
    settingsShow,
    userInfoShow,
    groupInfoShow,
    groupApplyShow,
    groupShow,
    addGroupMemberShow,
    editGroupInfoShow,
    groupMemberShow,
    forwardShow,

    // 方法
    $formatMime,
    $formatRatio,
    $formatTime,
    $formatStatic,
    $formatLocale,
    $formatFileIcon,
    $formatThumbnail,
    $t,
    $deepclone,
    $downloadFile,
    $closeAllAlert,
    $lodash: lodash,
    $toast: Toast,
    $dialog: Dialog,
  };
};
export const useHasLog = () => {
  const store = useStore();
  const { t: $t } = useI18n();
  const { proxy } = getCurrentInstance();
  const { $socket, $logout, $storage } = proxy;
  const socket = $socket.init();

  const chatMessages = computed(() => store.state.home.chatMessages);
  const loginUserInfo = computed(() => store.state.loginUserInfo);
  const chatList = computed(() => store.state.home.chatList);
  const selectChat = computed(() => store.state.home.selectChat);
  const friendInfo = computed(() => store.state.home.friendInfo);
  const groupInfo = computed(() => store.state.home.groupInfo);
  const friendApplyList = computed(() => store.state.home.friendApplyList);
  const myFriendApplyList = computed(() => store.state.home.myFriendApplyList);
  const friendList = computed(() => store.state.home.friendList);
  const groupList = computed(() => store.state.home.groupList);
  const groupApplyList = computed(() => store.state.home.groupApplyList);
  const groupMemberList = computed(() => store.state.home.groupMemberList);
  const groupManageList = computed(() => store.state.home.groupManageList);
  const lookUserInfo = computed(() => store.state.home.lookUserInfo);
  const lookType = computed(() => store.state.home.lookType);
  const topList = computed(() => store.state.home.topList);

  // 管理员
  const isManage = computed(() => {
    return (
      groupInfo.value?.memberInfo?.is_manage == IS_MANAGE &&
      groupInfo.value?.memberInfo?.is_black != IS_BLACK
    );
  });
  // 禁言
  const isForbidden = computed(() => {
    return groupInfo.value?.is_forbidden == IS_FORBIDDEN;
  });
  // 允许添加成员
  const isAddMember = computed(() => {
    return groupInfo.value?.is_add_member == IS_ADD_MEMBER;
  });
  // 允许显示昵称
  const isShownick = computed(() => {
    return groupInfo.value?.is_shownick == IS_SHOWNICK;
  });
  // 群主
  const isAdmin = computed(() => {
    return groupInfo.value?.memberInfo?.is_admin == IS_ADMIN;
  });
  // 开启了进群审核
  const isAudit = computed(() => {
    return groupInfo.value?.is_audit == IS_AUDIT;
  });
  // 允许转发
  const isForward = computed(() => {
    return groupInfo.value?.is_forward == IS_FORWARD;
  });
  // 可以处理入群申请
  const isSetApply = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_apply == IS_SET_APPLY;
  });
  // 可以封禁用户
  const isSetBlack = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_black == IS_SET_BLACK;
  });
  // 可以删除消息
  const isSetDelete = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_delete == IS_SET_DELETE;
  });
  // 可以设置管理员
  const isSetManage = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_manage == IS_SET_MANAGE;
  });
  // 可以发群公告
  const isSetNotice = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_notice == IS_SET_NOTICE;
  });
  // 可以修改群资料
  const isSetSetting = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_setting == IS_SET_SETTING;
  });
  // 可以设置置顶消息
  const isSetTop = computed(() => {
    return groupInfo.value?.manageInfo?.is_set_top == IS_SET_TOP;
  });
  // 群聊天
  const isGroupChat = computed(() => {
    return selectChat.value?.cate == "group";
  });
  // 好友聊天
  const isFriendChat = computed(() => {
    return selectChat.value?.cate == "friend";
  });
  // 昵称显示处理
  const $nickname = (info, member = null) => {
    let name = "";
    let flag = false;
    friendList.value.forEach((jtem) => {
      if (jtem.user_id == info.user_id) {
        flag = jtem;
      }
    });
    if (flag) {
      name = flag.remark || member?.gnickname || flag.nickname;
    } else {
      name = member?.gnickname || info.nickname;
    }
    return name;
  };
  // 显示name
  const showName = (current) => {
    if (current.fromInfo.user_id == loginUserInfo.value._id) {
      return $t("chat.myself");
    }
    if (current.fromMemberInfo) {
      if (current.fromMemberInfo.is_shownick == IS_SHOWNICK) {
        return $nickname(current.fromInfo, current.fromMemberInfo);
      }
    }
    return $nickname(current.fromInfo);
  }
  // 置顶消息类型
  const topMsgType = (msg_type, content) => {
    switch (msg_type) {
      case "image":
        return `🖼️${$t("chat.image")}`;
      case "audio":
        return `🎧${$t("chat.audio")}`;
      case "video":
        return `🎞️${$t("chat.video")}`;
      case "file":
        return `📄${$t("chat.file")}`;
      default:
        return `«${content}»`
    }
  }
  // 对系统消息处理显示
  const $formatSystemMsg = (item) => {
    try {
      // 获取返回的语言对象
      let language = $storage.get(LANGUAGE_OBJ);
      let content = item.content;
      let name = "", msg = "";
      if (item.fromInfo?.user_id == loginUserInfo.value._id) {
        name = $t("chat.myself");
      } else {
        name = showName(item);
      }
      if (language[content]) {
        msg = $formatLocale(language[content], "_name_", name);
      } else {
        msg = content;
      }
      // 置顶消息时
      if (content == "edit_setup_top") {
        let top = topMsgType(item.topInfo.msg_type, item.topInfo.content);
        return msg + `<font class="topping">${top}</font>`;
      }
      // 为好友时
      if (item.cate == "friend") {
        return language[content] || content;
      }
      // 为群组时
      else if (item.cate == "group") {
        if (
          content == "edit_group_info" ||
          content == "edit_group_open_forbidden" ||
          content == "edit_group_close_forbidden" ||
          content == "edit_group_open_audit" ||
          content == "edit_group_close_audit" ||
          content == "edit_group_permission" ||
          content == "edit_group_type_private" ||
          content == "edit_group_type_public" ||
          content == "edit_group_avatar"
        ) {
          return msg;
        } else if (
          content == "add_group_member" ||
          content == "add_group_manage" ||
          content == "exit_group"
        ) {
          let users = item.users.map(user => {
            let nickname = showName(user);
            return `<font class="msguser" data-id="${user.fromInfo.user_id}">${nickname}</font>`;
          })
          if (content == "exit_group") {
            return `<font class="msguser">${users[0]}</font> ${language[content]}`;
          }
          return msg + users;
        } else {
          if (content.indexOf("_create_group_") != -1) {
            let rep = content.replace("_create_group_", "");
            return `${name} ${$t("chat.add_group")} ${rep}`;
          }
          if (content.indexOf("_edit_group_name_") != -1) {
            let rep = content.replace("_edit_group_name_", "");
            return `${name} ${$t("chat.edit_group_name")} ${rep}`;
          }
          return language[content] || content;
        }
      }
    } catch (error) {
      console.log(error)
    }
  };
  // 获取我的用户信息
  const $getMyUserInfo = () => {
    socket.emit({
      type: "userinfo",
      data: {
        _id: loginUserInfo.value._id,
      },
    });
  }

  return {
    store,
    socket,
    // home vuex数据
    loginUserInfo,
    chatMessages,
    chatList,
    selectChat,
    friendInfo,
    groupInfo,
    friendApplyList,
    myFriendApplyList,
    friendList,
    groupList,
    groupApplyList,
    groupMemberList,
    groupManageList,
    lookUserInfo,
    lookType,
    topList,

    // 常量判断
    isAdmin,
    isManage,
    isForbidden,
    isAddMember,
    isShownick,
    isAudit,
    isForward,
    isSetApply,
    isSetBlack,
    isSetDelete,
    isSetManage,
    isSetNotice,
    isSetSetting,
    isSetTop,
    isGroupChat,
    isFriendChat,

    // 方法
    $logout,
    $nickname,
    $getMyUserInfo,
    $formatSystemMsg,
  };
};
