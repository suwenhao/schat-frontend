import defaultTheme from './default.json'
import darkTheme from './dark.json'

const setThemeDom = async (val, primary) => {
  let definedList = ["dark", "default"];
  let theme = "default";
  let styleText = "--transition-ms: 0.3s;";
  if (definedList?.indexOf(val) != -1) {
    theme = val;
  }
  let themeObj = {
    dark: darkTheme,
    default: defaultTheme
  }
  let themeJson = themeObj[theme];
  for (let key in themeJson) {
    styleText += `${key}: ${themeJson[key]};`;
  }
  if (primary) {
    styleText += `--color-primary: #${primary};--color-chat-active: #${primary};--color-links: #${primary};--color-own-links: #${primary};`;
  }
  document.querySelectorAll("html")[0].setAttribute("style", styleText);
};
export default setThemeDom;
