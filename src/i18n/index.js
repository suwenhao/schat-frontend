// 国际化
import store from "@/store";
import { createI18n } from "vue-i18n";
import { Locale } from "vant"

import ven_US from 'vant/es/locale/lang/en-US.mjs';
import vja_JP from 'vant/es/locale/lang/ja-JP.mjs';
import vzh_CN from 'vant/es/locale/lang/zh-CN.mjs';
import vzh_TW from 'vant/es/locale/lang/zh-TW.mjs';

import en_US from './lang/en-US.js';
import ja_JP from './lang/ja-JP.js';
import zh_CN from './lang/zh-CN.js';
import zh_TW from './lang/zh-TW.js';

export let locales = {
  'en-US': ven_US,
  'ja-JP': vja_JP,
  'zh-CN': vzh_CN,
  'zh-TW': vzh_TW,
};

let messages = {
  'en-US': en_US,
  'ja-JP': ja_JP,
  'zh-CN': zh_CN,
  'zh-TW': zh_TW,
};


const defaultLang = store.state.locale;
Locale.use(defaultLang, locales[defaultLang]);

const i18n = createI18n({
  legacy: false,
  locale: defaultLang, // set locale
  fallbackLocale: defaultLang,
  messages, // set locale messages
});
export { messages };
export default i18n;
