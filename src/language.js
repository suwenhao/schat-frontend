export default {
  "en-US": "English",
  "ja-JP": "日本語",
  "zh-CN": "简体中文",
  "zh-TW": "繁體中文",
}