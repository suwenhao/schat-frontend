import "babel-polyfill";
import * as Vue from "vue";
import "@/utils/websql";
// 懒加载模块
import VueLazy from "vue3-lazy";
import Toast from "vue-toastification";
import VCalendar from "v-calendar";
import VueVirtualScroller from "vue-virtual-scroller";
// 引入模块后自动生效
import "@vant/touch-emulator";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 引入主socket
import socket from "@/utils/socket";
import i18n from "@/i18n";
import install from "@/install";

// css
import "vant/lib/index.css";
import "animate.css/animate.min.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "material-icons/iconfont/material-icons.css";
import "@/assets/css/vue-toastification.css";
import "@/assets/css/index.scss";
import "plyr/dist/plyr.css";
import "vue-virtual-scroller/dist/vue-virtual-scroller.css";
import "photoswipe/style.css";
import "v-calendar/style.css";

import error from "@/assets/images/defunt_pic.png";

document.documentElement.style.webkitTouchCallout = "none";
const app = Vue.createApp(App);
app.use(VueVirtualScroller);
app.use(store).use(router).use(i18n).use(VCalendar, {}).use(socket, store);
app.use(Toast).use(install, store, router);
VueLazy.install(app, {});

app.mount("#app");
