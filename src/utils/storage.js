import { encrypt, decrypt } from "./crypto";

const deve = process.env.VUE_APP_ENV == "dev";

class StorageModel {
  /**
   * name
   */
  constructor(type) {
    this.type = type || "localStorage";
  }
  get(name) {
    if (this.type === "localStorage") {
      const data = localStorage.getItem(name) || "";
      let newData;
      try {
        const calcData = JSON.parse(
          deve ? data : decrypt(data)
        );
        if (Array.isArray(calcData)) {
          newData = [...calcData];
        } else if (calcData instanceof Object) {
          newData = { ...calcData };
        } else {
          newData = deve ? data : decrypt(data);
        }
      } catch (error) {
        newData = deve ? data : decrypt(data);
      }
      return newData;
    } else {
      const data = sessionStorage.getItem(name) || "";
      var newData;
      try {
        const calcData = JSON.parse(
          deve ? data : decrypt(data)
        );
        if (Array.isArray(calcData)) {
          newData = [...calcData];
        } else if (calcData instanceof Object) {
          newData = { ...calcData };
        } else {
          newData = deve ? data : decrypt(data);
        }
      } catch (error) {
        newData = deve ? data : decrypt(data);
      }
      return newData;
    }
  }
  set(name, data) {
    let newData;
    if (typeof data === "string") {
      newData = data || "";
    } else if (typeof data === "object") {
      newData = JSON.stringify(data) || "";
    } else {
      newData = data;
    }
    if (this.type === "localStorage") {
      return localStorage.setItem(
        name,
        deve ? newData : encrypt(newData)
      );
    } else {
      return sessionStorage.setItem(
        name,
        deve ? newData : encrypt(newData)
      );
    }
  }
  remove(name) {
    if (this.type === "localStorage") {
      return localStorage.removeItem(name);
    } else {
      return sessionStorage.removeItem(name);
    }
  }
  clear() {
    if (this.type === "localStorage") {
      localStorage.clear();
    } else {
      sessionStorage.clear();
    }
  }
}
export const Storage = new StorageModel();
export const SStorage = new StorageModel("sessionStorage");
export default StorageModel;
