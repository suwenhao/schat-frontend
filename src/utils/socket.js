import SocketIO from "socket.io-client";
import { TOKEN } from "@/utils/constant";
import { Storage, SStorage } from "@/utils/storage";
import SocketIoParser from "socket.io-msgpack-parser";

class Socket {
  constructor(store) {
    this.$store = store;
    this.protocol = location.protocol === "https:" ? "wss://" : "ws://";
    if (process.env.NODE_ENV === "production") {
      this.path = `${window.location.hostname}:${window.location.port}`;
    } else {
      this.path = `${process.env.VUE_APP_SOCKET_URL}`;
    }
  }
  init(path = "/ws") {
    if (!Socket.instance) {
      let socketOption = {
        path,
        transports: ["websocket"],
        autoConnect: false,
        forceNew: true,
        auth: {
          token: Storage.get(TOKEN),
        },
      };
      if (process.env.VUE_APP_ENV !== "dev") {
        socketOption.parser = SocketIoParser;
      }
      this.socket = SocketIO(`${this.protocol}${this.path}`, socketOption);
      this.socket.connect();
      this.socket.emit("HeartBeat");
      this.socket.on("HeartBeat", () => {
        clearTimeout(window.timer);
        window.timer = setTimeout(() => {
          this.socket.emit("HeartBeat");
        }, 1000 * 58);
      });
      this.socket.on("unauthorized", (callback) => {
        this.logout(callback);
      });
      this.socket.on("offline", (callback) => {
        this.logout(callback);
      });
      Socket.instance = this;
      return Socket.instance;
    } else {
      return Socket.instance;
    }
  }
  emit({ type, data = {}, callback }) {
    if (callback) {
      this.socket.emit(type, data, callback);
    } else {
      this.socket.emit(type, data);
    }
  }
  on({ type, callback = () => {} }) {
    this.socket.on(type, callback);
  }
  disconnect() {
    this.socket.disconnect();
  }
  logout(callback) {
    Storage.clear();
    SStorage.clear();
    this.socket.disconnect();
    callback();
    location.href = "/";
  }
}

export default {
  install(app, store) {
    app.config.globalProperties.$socket = new Socket(store);
  },
};
