import {
  preloadImage,
  preloadVideo,
  parseAudioMetadata,
  createPosterForVideo,
} from "./file";

export const SUPPORTED_IMAGE_CONTENT_TYPES = new Set(["image/jpeg", "image/png", "image/gif"]);
export const SUPPORTED_VIDEO_CONTENT_TYPES = new Set(["video/mp4", "video/ogg", 'video/webm']);
export const SUPPORTED_AUDIO_CONTENT_TYPES = new Set([
  'audio/mp3',
  'audio/ogg',
  'audio/wav',
  'audio/mpeg',
  'audio/flac',
  'audio/aac',
  'audio/m4a',
  'audio/mp4',
  'audio/x-m4a',
]);

export default async function buildAttachment(
  filename,
  blob,
  options={}
) {
  const blobUrl = window.URL.createObjectURL(blob);
  const { type: mimeType, size } = blob;
  let quick;
  let audio;
  let previewBlobUrl;

  if (SUPPORTED_IMAGE_CONTENT_TYPES.has(mimeType)) {
    const img = await preloadImage(blobUrl);
    const { width, height } = img;
    quick = { width, height };
    previewBlobUrl = blobUrl;
  } else if (SUPPORTED_VIDEO_CONTENT_TYPES.has(mimeType)) {
    const {
      videoWidth: width,
      videoHeight: height,
      duration,
    } = await preloadVideo(blobUrl);
    quick = { width, height, duration };
    
    previewBlobUrl = await createPosterForVideo(blobUrl);
  } else if (SUPPORTED_AUDIO_CONTENT_TYPES.has(mimeType)) {
    const { duration, title, performer, coverUrl } = await parseAudioMetadata(
      blobUrl
    );
    audio = {
      duration: duration || 0,
      title,
      performer,
    };
    previewBlobUrl = coverUrl;
  }
  return {
    blobUrl,
    filename,
    mimeType,
    size,
    quick,
    audio,
    previewBlobUrl,
    ...options,
  };
}
