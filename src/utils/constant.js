/**语言*/
export const LANGUAGE = "CHAT_LANGUAGE";
/**后端返回的语言文件*/
export const LANGUAGE_OBJ = "CHAT_LANGUAGE_OBJ"
/**主题*/
export const THEME = "CHAT_THEME";
/**桌面提示*/
export const DESKTOP_NOTICE = "CHAT_DESKTOP_NOTICE"
/**提示音*/
export const PLAY_BEEP = "CHAT_PLAY_BEEP"
/**主题色*/
export const PRIMARY = "CHAT_PRIMARY"
/**token*/
export const TOKEN = "CHAT_TOKEN";
/**用户信息*/
export const USERINFO = "CHAT_USERINFO";
/**好友信息*/
export const FRIEND_INFO = "CHAT_FRIEND_INFO";
/**群信息*/
export const GROUP_INFO = "CHAT_GROUP_INFO";
/**选择的聊天*/
export const SELECT_CHAT = "CHAT_SELECT_CHAT";
/**聊天列表*/
export const CHAT_LIST = "CHAT_CHAT_LIST";
/**为1*/
export const IS_YES = "1";
/**为2*/
export const IS_NO = "2";
/**@时*/
export const IS_AT = "1";
/**加我为好友时验证*/
export const IS_ADD_VERIFY = "1";
/**可以通过手机添加*/
export const IS_ADD_PHONE = "1";
/**可以通过账号添加*/
export const IS_ADD_USERNAME = "1"; 
/**可以通过昵称添加*/
export const IS_ADD_NICKNAME = "1";
/**不在线*/
export const IS_NOONLINE = "0";
/**在线*/
export const IS_ONLINE = "1";
/**隐身*/
export const IS_STEALTH = "2";
/**忙碌*/
export const IS_BUSY = "3";
/**离开*/
export const IS_LEAVE = "4";
/**男*/
export const IS_MALE = "1";
/**女*/
export const IS_FEMALE = "0";
/**同意申请*/
export const IS_APPROVE = "1";
/**拒绝申请*/
export const IS_REJECT = "2";
/**未设置申请*/
export const IS_NOAPPLY = "";
/**禁言*/
export const IS_FORBIDDEN = "1";
/**开启进群审核*/
export const IS_AUDIT = "1";
/**可以发送文本*/
export const IS_SENDTEXT = "1";
/**可以发送图片*/
export const IS_SENDIMAGE = "1";
/**可以发送媒体*/
export const IS_SENDMEDIA = "1";
/**可以发送文件*/
export const IS_SENDFILE = "1";
/**显示昵称*/
export const IS_SHOWNICK = "1";
/**可以添加成员*/
export const IS_ADD_MEMBER = "1";
/**可以转发*/
export const IS_FORWARD = "1";
/**禁用*/
export const IS_DISABLED = "1";
/**可以设置置顶消息*/
export const IS_SET_TOP = "1";
/**可以设置管理员*/
export const IS_SET_MANAGE = "1";
/**可以处理入群申请*/
export const IS_SET_APPLY = "1";
/**可以发群公告*/
export const IS_SET_NOTICE = "1";
/**可以修改群资料*/
export const IS_SET_SETTING = "1";
/**可以删除消息*/
export const IS_SET_DELETE = "1";
/**可以封禁用户*/
export const IS_SET_BLACK = "1";
/**是群主*/
export const IS_ADMIN = "1";
/**是管理员*/
export const IS_MANAGE = "1";
/**消息打扰*/
export const IS_DISTURB = "1";
/**置顶*/
export const IS_TOP = "1";
/**黑名单*/
export const IS_BLACK = "1";
/**私密群*/
export const IS_PRIVATE = "1";
/**公开群*/
export const IS_PUBLIC = "2";
/**已读*/
export const IS_READ = "1";
/**未读*/
export const IS_UNREAD = "2";
