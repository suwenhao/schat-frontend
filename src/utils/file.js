import path from "path-browserify";
import { delay } from "./index.js";

// blob 转 base64
export function blobToDataURL(blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const { result } = e.target || {};
      if (typeof result === "string") {
        resolve(result);
      }
      reject(new Error("Failed to read blob"));
    };
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}
// base64 转 blob
export function dataURLToBlob(dataUri) {
  const arr = dataUri.split(",");
  const mime = arr[0].match(/:(.*?);/)[1];
  const bstr = window.atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}
// blob 转 buffer
export function blobToArrayBuffer(blob) {
  return new Promise((resolve) => {
    let reader = new FileReader();
    reader.readAsArrayBuffer(blob);
    reader.onload = function (ev) {
      resolve(ev.target.result);
    };
  });
}
// blob 转 file
export function blobToFile(blob, fileName) {
  return new File([blob], fileName, {
    lastModified: Date.now(),
    type: blob.type,
  });
}
// blob 转 blob链接
export function blobToURL(blob) {
  let url = window.URL.createObjectURL(blob);
  return url;
}
export function urlToBlob(fileUrl) {
  return new Promise((resolve, reject) => {
    const x = new window.XMLHttpRequest()
    x.open('GET', fileUrl, true)
    x.responseType = 'blob' // 选择返回格式为blob
    x.onload = () => {
      let url = window.URL.createObjectURL(x.response) //将后端返回的blob文件读取出url
      resolve(url);
    }
    x.send()
  })
  
}
// blob链接 转 blob
export async function fetchBlob(blobUrl) {
  const response = await fetch(blobUrl);
  return response.blob();
}
// blob链接 转 file
export async function fetchFile(blobUrl, fileName) {
  const blob = await fetchBlob(blobUrl);
  return blobToFile(blob, fileName);
}
// 获取图片对象
export function preloadImage(url) {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve(img);
    img.onerror = reject;
    img.src = url;
  });
}
// 获取视频对象
export function preloadVideo(url) {
  return new Promise((resolve, reject) => {
    const video = document.createElement('video');
    video.volume = 0;
    video.onloadedmetadata = () => resolve(video);
    video.onerror = reject;
    video.src = url;
  });
}
// img 转 canvas
export function imgToCanvas(img) {
  const canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  const ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  return canvas;
}
// 获取视频宽高
export function getVideoSizeOf(blob) {
  return new Promise(async (resolve) => {
    let base64 = await blobToDataURL(blob);
    let video = document.createElement("video");
    video.src = base64;
    video.oncanplay = function (ev) {
      resolve({
        width: ev.target.videoWidth,
        height: ev.target.videoHeight,
      });
    };
  });
}
// 获取图片宽高
export function getImageSizeOf(blob) {
  return new Promise(async (resolve) => {
    let base64 = await blobToDataURL(blob);
    let img = document.createElement("img");
    img.src = base64;
    img.onload = function () {
      resolve({
        width: img.width,
        height: img.height,
      });
    };
  });
}
// 获取文件后缀名
export function fileSuffix(filename) {
  return path.extname(filename);
}
// 创建视频封面
export async function createPosterForVideo(url) {
  const video = await preloadVideo(url);
  return Promise.race([
    delay(2000),
    new Promise((resolve, reject) => {
      video.onseeked = () => {
        if (!video.videoWidth || !video.videoHeight) {
          resolve(undefined);
        }
        const canvas = document.createElement("canvas");
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        const ctx = canvas.getContext("2d");
        ctx.drawImage(video, 0, 0);
        resolve(canvas.toDataURL("image/jpeg"));
      };
      video.onerror = reject;
      video.currentTime = Math.min(video.duration, 1);
    }),
  ]);
}
// 解析音频元数据
export async function parseAudioMetadata(url) {
  const { fetchFromUrl, selectCover } = await import(
    "../lib/music-metadata-browser"
  );
  const metadata = await fetchFromUrl(url);
  const {
    common: { title, artist, picture },
    format: { duration },
  } = metadata;

  const cover = selectCover(picture);
  const coverUrl = cover
    ? `data:${cover.format};base64,${cover.data.toString("base64")}`
    : undefined;

  return {
    title,
    performer: artist,
    duration,
    coverUrl,
  };
}
// 缩放图像
export function scaleImage(image, ratio, outputType = "image/png") {
  const url = image instanceof Blob ? URL.createObjectURL(image) : image;
  const img = new Image();
  return new Promise((resolve) => {
    img.onload = () => {
      scale(img, img.width * ratio, img.height * ratio, outputType)
        .then((blob) => {
          if (!blob) throw new Error("Image resize failed!");
          return URL.createObjectURL(blob);
        })
        .then(resolve)
        .finally(() => {
          if (image instanceof Blob) {
            URL.revokeObjectURL(url); // Revoke blob url that we created
          }
        });
    };
    img.src = url;
  });
}
// 调整大小图像
export function resizeImage(image, width, height, outputType = "image/png") {
  const url = image instanceof Blob ? URL.createObjectURL(image) : image;
  const img = new Image();
  return new Promise((resolve) => {
    img.onload = () => {
      scale(img, width, height, outputType)
        .then((blob) => {
          if (!blob) throw new Error("Image resize failed!");
          return URL.createObjectURL(blob);
        })
        .then(resolve)
        .finally(() => {
          if (image instanceof Blob) {
            URL.revokeObjectURL(url); // Revoke blob url that we created
          }
        });
    };
    img.src = url;
  });
}
// 缩放方法
const scale = async (img, width, height, outputType = "image/png") => {
  if ("createImageBitmap" in window) {
    try {
      const bitmap = await window.createImageBitmap(img, {
        resizeWidth: width,
        resizeHeight: height,
        resizeQuality: "high",
      });
      if (bitmap.height !== height || bitmap.width !== width) {
        throw new Error("Image bitmap resize not supported!"); // FF93 added support for options, but not resize
      }
      return await new Promise((res) => {
        const canvas = document.createElement("canvas");
        canvas.width = bitmap.width;
        canvas.height = bitmap.height;
        const ctx = canvas.getContext("bitmaprenderer");
        if (ctx) {
          ctx.transferFromImageBitmap(bitmap);
        } else {
          canvas.getContext("2d").drawImage(bitmap, 0, 0);
        }
        canvas.toBlob(res, outputType);
      });
    } catch (e) {
      return steppedScale(img, width, height, undefined, outputType);
    }
  } else {
    return steppedScale(img, width, height, undefined, outputType);
  }
};
// 阶梯式刻度
export function steppedScale(
  img,
  width,
  height,
  step = 0.5,
  outputType = "image/png"
) {
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  const oc = document.createElement("canvas");
  const octx = oc.getContext("2d");

  canvas.width = width;
  canvas.height = height;

  if (img.width * step > width) {
    // For performance avoid unnecessary drawing
    const mul = 1 / step;
    let cur = {
      width: Math.floor(img.width * step),
      height: Math.floor(img.height * step),
    };

    oc.width = cur.width;
    oc.height = cur.height;

    octx.drawImage(img, 0, 0, cur.width, cur.height);

    while (cur.width * step > width) {
      cur = {
        width: Math.floor(cur.width * step),
        height: Math.floor(cur.height * step),
      };
      octx.drawImage(
        oc,
        0,
        0,
        cur.width * mul,
        cur.height * mul,
        0,
        0,
        cur.width,
        cur.height
      );
    }

    ctx.drawImage(
      oc,
      0,
      0,
      cur.width,
      cur.height,
      0,
      0,
      canvas.width,
      canvas.height
    );
  } else {
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
  }

  return new Promise((resolve) => {
    canvas.toBlob(resolve, outputType);
  });
}
