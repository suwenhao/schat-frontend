import CryptoJS from "crypto-js"; //引用AES源码js
const key = CryptoJS.enc.Utf8.parse("fEnYY1QPavanunx8qBByD1Hr5TJTusWk");
const iv = CryptoJS.enc.Utf8.parse(""); //偏移量

const decrypt = (word) => {
  try {
    //解密方法
    let base64 = CryptoJS.enc.Base64?.parse(word);
    let srcs = CryptoJS.enc.Base64?.stringify(base64);
    let decrypt = CryptoJS.AES?.decrypt(srcs, key, {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
    return decryptedStr.toString().replace(/[\r\n]/g, "");
  } catch (error) {
    return word;
  }
};

const encrypt = (word) => {
  try {
    //加密方法
    let srcs = CryptoJS.enc.Utf8?.parse(word);
    let encrypted = CryptoJS.AES.encrypt(srcs, key, {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    return CryptoJS.enc.Base64?.stringify(encrypted.ciphertext);
  } catch (error) {
    return word;
  }
};

export { decrypt, encrypt };

export default{
  decrypt, encrypt
}
