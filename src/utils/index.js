import moment from "moment";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import pinyin from "js-pinyin";
pinyin.setOptions({ checkPolyphone: false, charCase: 0 });
// 获取浏览器默认语言
export const getBrowserLang = () => {
  let browserLang = navigator.language
    ? navigator.language
    : navigator.browserLanguage;
  let defaultBrowserLang = "";
  if (browserLang.toLowerCase().indexOf("zh") != -1) {
    defaultBrowserLang = "zh-CN";
  } else {
    defaultBrowserLang = "en-US";
  }
  return defaultBrowserLang;
};
// 唯一设备id
export const uniqueIp = async () => {
  const fpPromise = FingerprintJS.load();
  const fp = await fpPromise;
  const result = await fp.get();
  const visitorId = result.visitorId;
  return visitorId;
};
// 时间
export const _formatDate = (date, fmt) => {
  var o = {
    "M+": date.getMonth() + 1, //月份
    "d+": date.getDate(), //日
    "h+": date.getHours(), //小时
    "m+": date.getMinutes(), //分
    "s+": date.getSeconds(), //秒
    "q+": Math.floor((date.getMonth() + 3) / 3), //季度
    S: date.getMilliseconds(), //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
      );
  return fmt;
};
// 计算图片等比
export const sizeWRatio = (w) => {
  if (w >= 1280) {
    return 464;
  } else if (w >= 1024 && w < 1280) {
    return 400;
  } else if (w >= 768 && w < 1024) {
    return 300;
  } else if (w >= 375 && w < 768) {
    return 280;
  } else if (w >= 320 && w < 375) {
    return 250;
  } else if (w >= 0 && w < 320) {
    return 220;
  } else {
    return 160;
  }
};
// sleep
export const delay = (ms) => {
  return new Promise((resolve) => { setTimeout(() => resolve(), ms); });
}
// 格式化时间
export const relativeTime = (
  timestamp,
  mustIncludeTime = true,
  locale = {
    justRecently: "刚刚",
    yesterday: "昨天",
    beforeday: "前天",
    week1: "星期一",
    week2: "星期二",
    week3: "星期三",
    week4: "星期四",
    week5: "星期五",
    week6: "星期六",
    week7: "星期日",
  }
) => {
  if (!timestamp) return "";
  var currentDate = new Date(); // 当前时间
  var srcDate = new Date(parseInt(timestamp)); //目标判断时间
  var currentYear = currentDate.getFullYear();
  var currentMonth = currentDate.getMonth() + 1;
  var currentDateD = currentDate.getDate();
  var srcYear = srcDate.getFullYear();
  var srcMonth = srcDate.getMonth() + 1;
  var srcDateD = srcDate.getDate();
  var ret = "";
  // 要额外显示的时间分钟
  var timeExtraStr = mustIncludeTime ? " " + _formatDate(srcDate, "hh:mm") : "";
  // 当年
  if (currentYear === srcYear) {
    var currentTimestamp = currentDate.getTime();
    var srcTimestamp = timestamp;
    var deltaTime = currentTimestamp - srcTimestamp;
    if (currentMonth === srcMonth && currentDateD === srcDateD) {
      ret = _formatDate(srcDate, "hh:mm");
    } else {
      var yesterdayDate = new Date();
      yesterdayDate.setDate(yesterdayDate.getDate() - 1);
      var beforeYesterdayDate = new Date();
      beforeYesterdayDate.setDate(beforeYesterdayDate.getDate() - 2);
      if (
        srcMonth === yesterdayDate.getMonth() + 1 &&
        srcDateD === yesterdayDate.getDate()
      )
        ret = locale.yesterday + timeExtraStr;
      // -1d
      else if (
        srcMonth === beforeYesterdayDate.getMonth() + 1 &&
        srcDateD === beforeYesterdayDate.getDate()
      )
        ret = locale.beforeday + timeExtraStr;
      // -2d
      else {
        var deltaHour = deltaTime / (3600 * 1000);
        // 如果小于或等 7*24小时就显示星期几
        if (deltaHour <= 7 * 24) {
          var weekday = new Array(7);
          weekday[0] = locale.week7;
          weekday[1] = locale.week1;
          weekday[2] = locale.week2;
          weekday[3] = locale.week3;
          weekday[4] = locale.week4;
          weekday[5] = locale.week5;
          weekday[6] = locale.week6;
          // 取出当前是星期几
          var weedayDesc = weekday[srcDate.getDay()];
          ret = weedayDesc + timeExtraStr;
        } else ret = _formatDate(srcDate, "yyyy-M-d") + timeExtraStr;
      }
    }
  } else ret = _formatDate(srcDate, "yyyy-M-d") + timeExtraStr;
  return ret;
};
// 获取手机还是pc
export const getNavigator = () => {
  var sUserAgent = navigator.userAgent.toLowerCase();
  var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
  var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
  var bIsMidp = sUserAgent.match(/midp/i) == "midp";
  var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
  var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
  var bIsAndroid = sUserAgent.match(/android/i) == "android";
  var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
  var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
  if (
    !(
      bIsIpad ||
      bIsIphoneOs ||
      bIsMidp ||
      bIsUc7 ||
      bIsUc ||
      bIsAndroid ||
      bIsCE ||
      bIsWM
    )
  ) {
    return "pc";
  } else {
    return "mobile";
  }
};
// 浏览器通知
export const browserWindow = (title, message) => {
  // 判断浏览器是否支持唤醒
  if (window.Notification) {
    let popNotice = () => {
      if (!Notification.permission === "granted") return;
      const notification = new Notification(title, {
        body: message,
      });
      // 点击通知的回调函数
      notification.onclick = function () {
        // window.open(location.href);
        notification.close();
      };
    };
    /* 授权过通知 */
    if (Notification.permission === "granted") {
      popNotice();
    } else {
      /* 未授权，先询问授权 */
      Notification.requestPermission(function (permission) {
        popNotice();
      });
    }
  }
};
// 返回头像背景颜色
export const avatarBgColor = (n='A') => {
  const list = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];
  let index = list?.indexOf(n) + 1;
  return "color" + Math.ceil(index / 3);
};
// 中文转字母
export const pinyinNameOne = (n) => {
  if (!n) return "";
  return pinyin.getFullChars(n).substring(0, 1).toUpperCase();
};
// 获取字符串中的链接
export const getStringWithUrl = (str) => {
  const reg =
    /(https?|http|ftp|file):\/\/[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g;
  const strValue = str.match(reg);
  if (strValue && strValue.length > 0) {
    return strValue[0];
  }
  return null;
};
// 格式化小时
export const formatHour = (num) => {
  if (num > 3600) {
    return (
      "0".repeat(2 - String(Math.floor(num / 3600)).length) +
      Math.floor(num / 3600) +
      ":" +
      "0".repeat(2 - String(Math.floor((num % 3600) / 60)).length) +
      Math.floor((num % 3600) / 60) +
      ":" +
      "0".repeat(2 - String(Math.floor((num % 3600) % 60)).length) +
      Math.floor((num % 3600) % 60)
    );
  }
  return (
    "0".repeat(2 - String(Math.floor((num % 3600) / 60)).length) +
    Math.floor((num % 3600) / 60) +
    ":" +
    "0".repeat(2 - String(Math.floor((num % 3600) % 60)).length) +
    Math.floor((num % 3600) % 60)
  );
};
