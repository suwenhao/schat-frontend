import localforage from "localforage";
import {
  LANGUAGE,
  THEME,
  DESKTOP_NOTICE,
  PLAY_BEEP,
  PRIMARY,
} from "@/utils/constant";
import { encrypt, decrypt } from "@/utils/crypto";
import { getBrowserLang } from "@/utils"

const deve = process.env.VUE_APP_ENV == "dev";

localforage.config({
  driver: localforage.INDEXEDDB, // Force WebSQL; same as using setDriver()
  name: "schat",
  version: 2.0,
  size: 4980736, // Size of database, in bytes. WebSQL-only for now.
  storeName: "keyvaluepairs", // Should be alphanumeric, with underscores.
  description: "schatDB",
});
let sqlStore = localforage.createInstance({
  name: "schat_" + process.env.VUE_APP_VERSION,
});
class Websql {
  constructor() {
    this.keys();
  }
  async get(name) {
    let data = (await sqlStore.getItem(name)) || "";
    let newData;
    try {
      const calcData = JSON.parse(deve ? data : decrypt(data));
      if (Array.isArray(calcData)) {
        newData = [...calcData];
      } else if (calcData instanceof Object) {
        newData = { ...calcData };
      } else {
        newData = deve ? data : decrypt(data);
      }
    } catch (error) {
      newData = deve ? data : decrypt(data);
    }
    return newData;
  }
  async set(name, obj) {
    let newData;
    if (typeof obj === "string") {
      newData = obj || "";
    } else if (typeof obj === "object") {
      newData = JSON.stringify(obj) || "";
    } else {
      newData = obj;
    }
    return await sqlStore.setItem(name, deve ? newData : encrypt(newData));
  }
  async remove(name) {
    return await sqlStore.removeItem(name);
  }
  async clear() {
    return await sqlStore.clear();
  }
  async keys() {
    return await sqlStore.keys();
  }
}
export const websql = new Websql();
(async () => {
  let locale = await websql.get(LANGUAGE);
  let theme = await websql.get(THEME);
  let desknotice = await websql.get(DESKTOP_NOTICE);
  let playbeep = await websql.get(PLAY_BEEP);
  let primary = await websql.get(PRIMARY);
  if (!locale) {
    await websql.set(LANGUAGE, getBrowserLang() || process.env.VUE_APP_LANGUAGE);
  }
  if (!theme) {
    await websql.set(THEME, process.env.VUE_APP_THEME);
  }
  if (!desknotice) {
    await websql.set(DESKTOP_NOTICE, process.env.VUE_APP_DESKTOP_NOTICE);
  }
  if (!playbeep) {
    await websql.set(PLAY_BEEP, process.env.VUE_APP_PLAY_BEEP);
  }
  if (!primary) {
    await websql.set(PRIMARY, process.env.VUE_APP_PRIMARY);
  }
})();
